#include "starbugs_coffee.hpp"
#include <memory>

void client(std::unique_ptr<Coffee> coffee)
{
    std::cout << "Description: " << coffee->get_description() << "; Price: " << coffee->get_total_price() << std::endl;
    coffee->prepare();
}

class CoffeeBuilder
{
    std::unique_ptr<Coffee> coffee_;
public:
    template<typename T>
    CoffeeBuilder& create_base()
    {
        coffee_ = std::make_unique<T>();

        return *this;
    }

    template <typename T>
    CoffeeBuilder& add()
    {
        coffee_ = std::make_unique<T>(std::move(coffee_));

        return *this;
    }

    std::unique_ptr<Coffee> get_coffee()
    {
        return std::move(coffee_);
    }
};

int main()
{
    std::unique_ptr<Coffee> cf =
        std::make_unique<Whipped>(
            std::make_unique<Whisky>(
                std::make_unique<Whisky>(
                    std::make_unique<Whisky>(
                        std::make_unique<Whisky>(
                            std::make_unique<Espresso>())))));

    CoffeeBuilder coffee_bld;

    coffee_bld.create_base<Espresso>()
        .add<Whisky>()
        .add<Whipped>();

    client(std::move(coffee_bld.get_coffee()));
}
