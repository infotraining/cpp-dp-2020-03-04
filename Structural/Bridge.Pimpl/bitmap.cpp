#include "bitmap.hpp"
#include <algorithm>
#include <array>
#include <deque>

using namespace std;

struct Bitmap::BitmapImpl
{
    BitmapImpl(size_t size, uint8_t fill_char) : image_(size, fill_char)
    {}

    std::deque<uint8_t> image_;
};

Bitmap::Bitmap(size_t size, char fill_char)
    //: impl_{std::make_unique<BitmapImpl>(size, fill_char)}
    : impl_{new BitmapImpl(size, fill_char)}
{    
}

Bitmap::~Bitmap() = default;

void Bitmap::draw()
{
    cout << "Image: ";
    for (size_t i = 0; i < impl_->image_.size(); ++i)
        cout << impl_->image_[i];
    cout << endl;
}
