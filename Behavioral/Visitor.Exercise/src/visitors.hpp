#ifndef VISITORS_HPP
#define VISITORS_HPP

#include "ast.hpp"
#include <iostream>
#include <sstream>

class ExprEvalVisitor : public AST::AstVisitor
{
    int result_{};

public:
    void visit(AST::AddNode& node)
    {
        ExprEvalVisitor lv, rv;
        node.left().accept(lv);
        node.right().accept(rv);
        result_ = lv.result() + rv.result();
    }

    void visit(AST::MultiplyNode& node)
    {
        ExprEvalVisitor lv, rv;
        node.left().accept(lv);
        node.right().accept(rv);
        result_ = lv.result() * rv.result();
    }

    void visit(AST::IntNode& node)
    {
        result_ = node.value();
    }

    int result() const
    {
        return result_;
    }
};

class PrintingVisitor : public AST::AstVisitor
{
    std::stringstream printer_;
public:
    void visit(AST::AddNode &node) override
    {
        node.left().accept(*this);
        printer_ << " + ";
        node.right().accept(*this);
    }

    void visit(AST::MultiplyNode &node) override
    {
        node.left().accept(*this);
        printer_ << " * ";
        node.right().accept(*this);
    }

    void visit(AST::IntNode &node) override
    {
        printer_ << node.value();
    }

    std::string str() const
    {
        return printer_.str();
    }
};

#endif // VISITORS_HPP
