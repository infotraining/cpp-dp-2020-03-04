#include <algorithm>
#include <fstream>
#include <iostream>
#include <iterator>
#include <list>
#include <numeric>
#include <stdexcept>
#include <string>
#include <vector>
#include <memory>
#include <functional>

struct StatResult
{
    std::string description;
    double value;

    StatResult(const std::string& desc, double val) : description(desc), value(val)
    {
    }
};

using Data = std::vector<double>;
using Results = std::vector<StatResult>;

class Statistics
{
public:
    virtual Results calculate(const Data& data) = 0 ;
    virtual ~Statistics() = default;
};

namespace ModernCpp
{
    using Statistics = std::function<Results (const Data& data)>;

    class Avg
    {
    public:
        Results operator()(const Data &data)
        {
            double sum = std::accumulate(data.begin(), data.end(), 0.0);
            double avg = sum / data.size();

            return { {"AVG", avg} };
        }
    };

    class DataAnalyzer
    {
        Statistics statistics_;
        Data data_;
    public:

        void set_statistics(Statistics new_stat)
        {
            statistics_ = new_stat;
        }

        void calculate()
        {
            auto results = statistics_(data_); // delegation to strategy
        }
    };
}

class Avg : public Statistics
{
    // Statistics interface
public:
    Results calculate(const Data &data) override
    {
        double sum = std::accumulate(data.begin(), data.end(), 0.0);
        double avg = sum / data.size();

        return { {"AVG", avg} };
    }
};

class MinMax : public Statistics
{
    // Statistics interface
public:
    Results calculate(const Data &data) override
    {
        double min = *(std::min_element(data.begin(), data.end()));
        double max = *(std::max_element(data.begin(), data.end()));

        return { {"MIN", min}, {"MAX", max } };
    }
};

class Sum : public Statistics
{
    // Statistics interface
public:
    Results calculate(const Data &data) override
    {
        double sum = std::accumulate(data.begin(), data.end(), 0.0);

        return { {"SUM", sum} };
    }
};

class StatGroup : public Statistics
{
    std::vector<std::shared_ptr<Statistics>> stats_;
public:
    void add(std::shared_ptr<Statistics> stat)
    {
        stats_.push_back(stat);
    }

    Results calculate(const Data &data) override
    {
        Results grouped_results;

        for(const auto& stat : stats_)
        {
            auto results = stat->calculate(data);
            grouped_results.insert(grouped_results.end(), results.begin(), results.end());
        }

        return grouped_results;
    }
};

class DataAnalyzer
{
    std::shared_ptr<Statistics> statistics_;
    Data data_;
    Results results_;

public:
    DataAnalyzer(std::shared_ptr<Statistics> stat) : statistics_{stat}
    {
    }

    void load_data(const std::string& file_name)
    {
        data_.clear();
        results_.clear();

        std::ifstream fin(file_name.c_str());
        if (!fin)
            throw std::runtime_error("File not opened");

        double d;
        while (fin >> d)
        {
            data_.push_back(d);
        }

        std::cout << "File " << file_name << " has been loaded...\n";
    }

    void set_statistics(std::shared_ptr<Statistics> stat)
    {
        statistics_ = stat;
    }

    void calculate()
    {
        auto stats = statistics_->calculate(data_);

        results_.insert(results_.end(), stats.begin(), stats.end());
    }

    const Results& results() const
    {
        return results_;
    }
};

void show_results(const Results& results)
{
    for (const auto& rslt : results)
        std::cout << rslt.description << " = " << rslt.value << std::endl;
}

int main()
{
    auto avg = std::make_shared<Avg>();
    auto min_max = std::make_shared<MinMax>();
    auto sum = std::make_shared<Sum>();

    auto std_stats = std::make_shared<StatGroup>();
    std_stats->add(avg);
    std_stats->add(min_max);
    std_stats->add(sum);

    DataAnalyzer da{std_stats};
    da.load_data("data.dat");
    da.calculate();
    show_results(da.results());

    std::cout << "\n\n";

    da.load_data("new_data.dat");
    da.calculate();
    show_results(da.results());
}
