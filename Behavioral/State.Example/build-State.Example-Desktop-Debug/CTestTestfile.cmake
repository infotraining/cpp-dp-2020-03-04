# CMake generated Testfile for 
# Source directory: /home/dev/training/cpp-dp-2020-03-04/Behavioral/State.Example
# Build directory: /home/dev/training/cpp-dp-2020-03-04/Behavioral/State.Example/build-State.Example-Desktop-Debug
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
subdirs("src")
subdirs("tests")
