#include <algorithm>
#include <numeric>
#include <iostream>
#include <string>
#include <vector>
#include <variant>

#include "catch.hpp"

using namespace std;

TEST_CASE("variant")
{
    std::variant<int, double, std::string> var;

    REQUIRE(var.index() == 0);
    REQUIRE(std::holds_alternative<int>(var));
    REQUIRE(std::get<int>(var) == 0);

    int* ptr_int = std::get_if<int>(&var);
    REQUIRE(ptr_int != nullptr);


    var = "abc"s;

    REQUIRE(var.index() == 2);
    REQUIRE(std::holds_alternative<std::string>(var));
    REQUIRE(std::get<std::string>(var) == "abc"s);

    ptr_int = std::get_if<int>(&var);
    REQUIRE(ptr_int == nullptr);
}

struct Rectangle
{
    int w, h;
};

struct Circle
{
    int radius;
};

struct Square
{
    int size;
};

using Shape = std::variant<Rectangle, Circle, Square>;

struct ShapePrinter
{
    void operator()(const Rectangle& r) const
    {
        std::cout << "Rectangle: " << r.w << " " << r.h << "\n";
    }

    void operator()(const Circle& c) const
    {
        std::cout << "Circle: " << c.radius << "\n";
    }

    void operator()(const Square& s) const
    {
        std::cout << "Square: " << s.size << "\n";
    }
};

template <typename... Ts>
struct overload : Ts...
{
    using Ts::operator()...;
};

template <typename... Ts>
overload(Ts...) -> overload<Ts...>;

TEST_CASE("static polymorhism with variant")
{
    SECTION("visiting single variant")
    {
        Shape s = Circle{100};

        std::visit(ShapePrinter{}, s);
    }

    vector<Shape> shapes;
    shapes.push_back(Rectangle{100, 200});
    shapes.push_back(Circle{20});
    shapes.push_back(Square{300});

    ShapePrinter shape_prn;
    for(const auto& s : shapes)
    {
        std::visit(shape_prn, s);
    }

    double area{};

    auto area_calculator = overload {
        [](const Rectangle& r) -> double { return r.w * r.h;}, // Lambda1
        [](const Circle& c) -> double { return c.radius * c.radius * 3.14; }, // Lambda2
        [](const Square& s) -> double { return s.size * s.size; } // Lambda3
    };

    for(const auto& s : shapes)
    {
        area += std::visit(area_calculator, s);
    }

    std::cout << "Area: " << area << "\n";
}
