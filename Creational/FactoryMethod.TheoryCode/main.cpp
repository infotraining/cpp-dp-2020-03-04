#include <cstdlib>
#include <iostream>
#include <string>
#include <unordered_map>
#include <vector>
#include <list>

#include "factory.hpp"
#include <functional>

using namespace std;

using LoggerCreator = std::function<std::unique_ptr<Logger>()>;

class Service
{
    LoggerCreator creator_;

public:
    Service(LoggerCreator creator) : creator_(creator)
    {
    }

    Service(const Service&) = delete;
    Service& operator=(const Service&) = delete;

    void use()
    {
        unique_ptr<Logger> logger = creator_();
        logger->log("Client::use() has been started...");
        run();
        logger->log("Client::use() has finished...");
    }
protected:
    virtual void run() {}
};

using LoggerFactory = std::unordered_map<std::string, LoggerCreator> ;

int main()
{
    LoggerFactory logger_factory;
    logger_factory.insert(make_pair("ConsoleLogger", &make_unique<ConsoleLogger>));
    logger_factory.insert(make_pair("FileLogger", [] { return std::make_unique<FileLogger>("data.log"); }));
    logger_factory.insert(make_pair("DbLogger", [] { return std::make_unique<DbLogger>("db.com"); }));

    Service srv(logger_factory.at("DbLogger"));
    srv.use();
}
