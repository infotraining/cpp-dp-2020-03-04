#include "circle.hpp"
#include "shape_factories.hpp"

using namespace std;
using namespace Drawing;

namespace {
    bool is_registered =
        Drawing::SingletonShapeFactory::instance()
            .register_creator(Circle::id, &make_unique<Circle>);
}

int Circle::radius() const
{
    return radius_;
}

void Circle::set_radius(int radius)
{
    radius_ = radius;
}
