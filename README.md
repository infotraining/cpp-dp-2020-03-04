# Design Patterns in C++

## Docs

* https://infotraining.bitbucket.io/cpp-dp
* https://gitpitch.com/infotraining-dev/cpp-dp/develop?grs=bitbucket&p=slides_pl

## Libraries

* https://github.com/facebook/folly/tree/master/folly
* https://github.com/abseil/abseil-cpp

## VCPKG

```
mkdir build
cd build
cmake .. -DCMAKE_TOOLCHAIN_FILE="~/libs/vcpkg/scripts/buildsystems/vcpkg.cmake"
make
```

## Ankieta

* https://www.infotraining.pl/ankieta/cpp-dp-2020-03-04-kp
